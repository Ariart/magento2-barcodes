<?php
//First required file after config import.
$logFile = null;

if(!is_dir(dirname(__DIR__)."/data")) mkdir(dirname(__DIR__)."/data");
$htmlCacheDir = $config["cache"]["html_cache"] ?? dirname(__DIR__)."/html_cache";
$quiet = !$confs["logs"]["enabled"] ?? false;
$logFilePath = $confs["logs"]["path"] ?? "/tmp/barcodes.log";
if(!is_dir($htmlCacheDir)) mkdir($htmlCacheDir,0777,true);
if(!$quiet && is_dir(dirname($logFilePath)) && is_readable(dirname($logFilePath))){
	$logFile = fopen($confs["logs"]["path"],"a");
	if(!is_resource($logFile)) throw new Exception("Unable to open ".$confs["logs"]["path"]);
}

define("AJAX","ajax");
define("GTIN","gtin");
define("CSRF","csrfToken");
define("RETRIEVED_PRODUCTS", "retrieved_product");

define("ACTION","action");
define("ACTION_CONNECT","connect");
define("ACTION_CHECK","check");
define("ACTION_CREATE","create");
define("ACTION_IMPORT","import");
define("ACTION_KEEP_ALIVE","keep_alive");
define("ACTION_CLEAR_CACHE","clear_cache");
define("ACTION_CATEGORIES","categories");

define("ERR_403",["code" => 403, "data" => "Must be logged !"]);
define("ERR_404",["code" => 404, "data" => "Not found !"]);
define("ERR_500",["code" => 500, "data" => "Internal Error"]);
define("WARN",$confs["logs"]["WARN"] ?? [ "color" => 33, "text" => "WARN" ]); //33 : orange
define("LOG", $confs["logs"]["LOG"] ?? [ "color" => 0, "text" => "INFO" ]);//0 : default
define("ERR", $confs["logs"]["ERR"] ?? [ "color" => 31, "text" => "ERR" ]);//31 : rouge
define("FATAL", $confs["logs"]["FATAL"] ?? [ "color" => 41, "text" => "FATAL ERROR" ]);//41 : background rouge
define("OK", $confs["logs"]["OK"] ?? ["color" => 32, "text" => "SUCESS"]);//32 : vert
define("LOG_TEMPLATE", $confs["logs"]["LOG_TEMPLATE"] ?? "\e[@colorm[@date] [@type] @message\e[0m\n");

/**
 * Sanitize a string
 * @param string $str
 * @return string
 */
function sanitizeString(string $str) : string{
	$res = str_replace("\n","",strip_tags($str));
	$res = preg_replace("/ {2,}/"," ",$res);
	return trim($res);
}

$log = function(string $message, array $params=LOG) use (&$logFile, &$quiet){
	$handles = [];
	if(is_resource($logFile)) $handles[] = $logFile;
	foreach($handles as $h) fwrite($h,str_replace(
		["@color", "@date", "@type", "@message", "@ip"],
		[
			$params["color"],
			\DateTime::createFromFormat('0.u00 U',microtime())
			         ->setTimezone(new \DateTimeZone(date_default_timezone_get()))
			         ->format("d-m-Y H:i:s.u"),
			$params["text"],
			$message,
			$_SERVER["REMOTE_ADDR"] ?? "no-ip"
		],
		LOG_TEMPLATE
	));
};

set_error_handler(function(string $errno, string $errstr, string $errfile, string $errline) use (&$log){
	$log(new ErrorException($errstr, 0, $errno, $errfile, $errline),FATAL);
	exit(1);
},E_ALL);

/**
 * Send a json error reponse if a get param 'ajax' is specified is the request,
 * redirect to root otherwise.
 * @param array $params [code => string, data => mixed]
 * @param array $errType [color => string, text => string] see $log
 */
$error = function (array $params, array $errType = ERR) use (&$log){
	$string = json_encode($params);
	if($_GET[AJAX] ?? false) echo $string;
	else header("Location: /");
	$log($string,$errType);
	exit(0);
};

/**
 * Send a JSON response body if a get param 'ajax' is specified is the request,
 * redirect to root on error otherwise.
 * @param string $code Response code
 * @param mixed  $data Data to send
 */
$respond = function(string $code, $data) use (&$log, &$error):void{
	$string = json_encode([ "code" => $code, "data" => $data ]);
	if($_GET[AJAX] ?? false) echo $string; else $error(ERR_500);
	$log("Response sent : $string",OK);
	exit(0);
};

/**
 * Cache an HTML page
 * @param string      $url      Page url
 * @param string      $html     Page content
 * @param null|string $subdirs  Subdir where to save the cached page
 */
$cacheHTML = function(string $url, string $html, ?string $subdirs = null) use (&$htmlCacheDir,&$log):void{
	file_put_contents("$htmlCacheDir/".($subdirs?"$subdirs/":"").sha1($url),$html);
	$log("$url cached.",OK);
};
/**
 * Retrieve an HTML page from cache, if any
 * @param string      $url     Page url
 * @param null|string $subdirs Subdir where the page is supposed to be cached
 * @return null|string         Page content, if any.
 */
$fetchHTMLFromCache = function(string $url, ?string $subdirs = null) use (&$htmlCacheDir,&$log, &$confs):?string{
	$fPath = "$htmlCacheDir/".($subdirs?"$subdirs/":"").sha1($url);
	if(file_exists($fPath) && microtime(true) - filemtime($fPath) < $confs["cache"]["max_html_lifetime"] ?? 86400){
		$res = file_get_contents("$htmlCacheDir/".sha1($url));
		$log("$url fetched from cache.");
		return $res;
	} else return false;
};
$clearHTMLCache = function() use (&$htmlCacheDir,&$log):void{
	if(is_dir($htmlCacheDir)){
		exec("rm -rf \"$htmlCacheDir\"");
		mkdir($htmlCacheDir);
		$log("HTML cache cleared.");
	}
};

/**
 * Download the file pointed out by $url into $dest
 *
 * @param string        $url              URL to the remote file
 * @param string        $dest             File destination
 * @param callable|null $curlOptParameter a callable that accept as first argument the curl ressource
 *                                        and return a curl ressource (can be the same) or null.
 * @return null|string
 */
$downloadRemoteFile = function(string $url, ?string $dest = null, ?callable $curlOptParameter=null):?string{
	if($dest) $fp = fopen ($dest, 'w+');// open file handle
	else $fp = null;

	$ch = curl_init($url);
	if(is_resource($fp)) curl_setopt($ch, CURLOPT_FILE, $fp);// output to file
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 1000);// some large value to allow curl to run for a long time
	curl_setopt($ch, CURLOPT_ENCODING , "");
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
	curl_setopt($ch, CURLOPT_HTTPHEADER,[
		"accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"accept-language: fr-FR,fr;q=0.9",
		"cache-control: no-cache",
		"pragma: no-cache",
		"sec-fetch-mode: navigate",
		"sec-fetch-site: none",
		"sec-fetch-user: ?1",
		"upgrade-insecure-requests: 1",
		"user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36"
	]);
	if(is_callable($curlOptParameter)) $ch = $curlOptParameter($ch) ?? $ch;
	if(!is_resource($fp)) curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	$res = curl_exec($ch);

	curl_close($ch);// closing curl handle
	if(is_resource($fp)) fclose($fp);// closing file handle
	else return $res;
	return null;
};

$additionnalValidationKeys = [];

/**
 * Validate inputs ($_GET and $_POST) and sanitize.
 * @param string ...$keys Keys to sanitize and validate
 */
$validateAndSanitize = function(string... $keys) use (&$error,&$additionnalValidationKeys) : void{
	foreach($keys as $key){
		switch($key){
			case ACTION :
				$action = $_GET[ACTION] ?? null;
				if(!$action) $error(["code"=> 404, "data" => "An action must be specified !"],WARN);
				break;
			case CSRF :
				$csrf = $_GET[CSRF] ?? null;
				$action = $_GET[ACTION] ?? null;
				if($action !== ACTION_CONNECT && (!$csrf || $csrf !== ($_SESSION[CSRF] ?? null)))
					$error(["code"=>"400","data"=>"Invalid csrf token supplied !"], WARN);
				break;
			case GTIN :
				$gtin = $_GET[GTIN] ?? null;
				if(!is_string($gtin) || !preg_match("/^[0-9]{4,20}$/",$gtin))
					$error([ "code" => 400, "data" => "A gtin must be specified (GET) and must be a number (min length : 4, max length : 20) !" ]);
				break;
			default :
				if(isset($additionnalValidationKeys[$key])) $additionnalValidationKeys[$key]();
				else $error([ "code" => 400, "data" => "Unknown data key : $key"]);
				break;
		}
	}
};

/**
 * @param string $str
 * @return string
 */
function urlKey(string $str):string{
	$str = htmlentities($str, ENT_NOQUOTES, "utf-8");
	$str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
	$str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
	$str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
	$str = preg_replace('/[^a-z0-9_\/-]+/i', "-",$str);
	$str = preg_replace("/(-|_){2,}/","-",$str);
	$str = substr($str,0,64);
	return $str;
};