<?php

require_once __DIR__."/GTINMap.php";

define("CSV_MAX_SIZE",104857600); // 100MB /!\ May depend on the serveur configuration !
define("CSV_MIME",["/^text\/csv$/i","/^application\/vnd\.ms-excel$/i","/^text\/plain$/i"]);
define("IMPORT_CSV","import_csv");
define("CSV_DELIMITER","csv_delimiter");
define("CSV_SKU_INDEX","csv_sku_index");
define("CSV_GTIN_INDEX","csv_gtin_index");

$additionnalValidationKeys[IMPORT_CSV] = function()use(&$error,&$log){
	$data = $_FILES[IMPORT_CSV] ?? null;
	do{
		if(!is_array($data)) continue;
		if(!isset($data["tmp_name"])) continue;
		if(!file_exists($data["tmp_name"]))
			$error(["code"=>500,"data"=>"An unknown error occured while trying to upload your file."]);
		if(CSV_MAX_SIZE >= 0){
			if(CSV_MAX_SIZE < filesize($data["tmp_name"])) $error(["code"=>400,"data"=>"File too large ! (Max allowed : 100Mb)"]);
		}
	}while(false);
	$ok = false;
	$mime = mime_content_type($data["tmp_name"]);
	foreach(CSV_MIME as $rule) if(preg_match($rule,$mime)) $ok = true;
	if(!$ok) $error(["code" => 400, "data" => "Invalid file ! (Given type : $mime instead of text/csv)"]);
};
$additionnalValidationKeys[CSV_DELIMITER] = function(){
	$delimiter = $_POST[CSV_DELIMITER] ?? null;
	if(is_string($delimiter) && !preg_match("/^.{1,255}$/",$delimiter))
		$error(["code"=>400,"A delimiter must be a string smaller than 255 chars !"]);
};
$additionnalValidationKeys[CSV_GTIN_INDEX] = function(){
	$index = $_POST[CSV_GTIN_INDEX] ?? null;
	if(is_string($index) && !preg_match("/^[0-9]+$/",$index))
		$error(["code"=>400,"The GTIN column index must be an integer !"]);
};
$additionnalValidationKeys[CSV_SKU_INDEX] = function(){
	$index = $_POST[CSV_SKU_INDEX] ?? null;
	if(is_string($index) && !preg_match("/^[0-9]+$/",$index))
		$error(["code"=>400,"The SKU column index must be an integer !"]);
};

/**
 * Import a csv file into the GTIN map.
 * @param string $file
 * @param string $delimiter
 * @param int    $gtinIndex
 * @param int    $skuIndex
 */
$importGTINMap = function(
	string $file,
	string $delimiter = ",",
	int $gtinIndex = 0,
	int $skuIndex = 1
) use (&$log, &$GTINToSKU, &$SKUToGTIN, &$saveGTINMap) {
	if($gtinIndex === $skuIndex) $error(["code"=>400,"data"=>"GTIN index and SKU index can't be the same !"]);
	$log("Starting import of $file (delimiter : $delimiter, gtin index : $gtinIndex, sku index : $skuIndex)");
	$current = 0;
	$invalidLines = 0;
	$importedLines = 0;
	$handle = fopen($file,"r");
	while(!feof($handle)){
		$l = explode($delimiter,$raw = rtrim(fgets($handle), "$delimiter\t\n\r\0\x0B"));
		if(!isset($l[$gtinIndex])){
			$log("Undefined column $gtinIndex (GTIN) line $current.",WARN);
			$invalidLines++;
			$current++;
			continue;
		}
		$gtin = htmlentities(strip_tags($l[$gtinIndex]));

		if(!isset($l[$skuIndex])){
			$log("Undefined column $skuIndex (SKU) line $current",WARN);
			$invalidLines++;
			$current++;
			continue;
		}
		$sku = htmlentities(strip_tags($l[$skuIndex]));

		if(preg_match("/^[0-9]{4,20}$/",$gtin) && preg_match("/^[^\"]{1,255}$/",$sku)){
			$GTINToSKU[$gtin] = $sku;
			$SKUToGTIN[$sku] = $gtin;
			$importedLines++;
		}else{
			$log("Invalid line $current (invalid GTIN or SKU) : \"$raw\"",ERR);
			$invalidLines ++;
		}
		$current++;
	}
	$saveGTINMap();
	$log("File $file successfully imported ! (imported : $importedLines, ignored : $invalidLines, total : $current)",OK);
};