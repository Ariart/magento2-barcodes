<?php

require_once dirname(__DIR__)."/lib/createProductFrom.fn.php";

$validateAndSanitize(GTIN, TITLE, DESC, PRICE, BRAND, SKU, CATEGS, QTT);
$product = $createProductFrom(
	$_objectManager,
	$_GET[GTIN],
	$_POST[TITLE],
	$_POST[DESC],
	$_POST[PRICE],
	$_POST[BRAND],
	$_POST[CHOOSEN] ?? null,
	$_POST[SKU] ?? null,
	$_POST[CATEGS] ?? null,
	$_POST[QTT] ?? 1
);
if(!$product) $error(ERR_500,FATAL); else{
	$urlHelper = $_objectManager->get('\Magento\Backend\Helper\Data');
	$respond("001",$urlHelper->getUrl(
		'catalog/product/edit',
		[ 'id' => $product->getId() ]
	));
}