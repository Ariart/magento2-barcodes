<?php

require_once dirname(__DIR__)."/lib/searchProductOnline.fn.php";
require_once dirname(__DIR__)."/lib/GTINMap.php";

$validateAndSanitize(GTIN);
$gtin = $_GET[GTIN];

$existingSKU = $getSKUByGTIN($gtin);
if($existingSKU){
	$urlHelper = $_objectManager->get('\Magento\Backend\Helper\Data');
	try{
		$product = $_objectManager->create('Magento\Catalog\Api\ProductRepositoryInterface')->get($existingSKU);
		if($product) $respond("001",$urlHelper->getUrl(
			'catalog/product/edit',
			[ 'id' => $product->getId() ]
		));
	}catch(Error | Exception $e){}
}else{
	try{
		$product = $_objectManager->create('Magento\Catalog\Api\ProductRepositoryInterface')->get($gtin);
		if($product) $respond("001",$urlHelper->getUrl(
			'catalog/product/edit',
			[ 'id' => $product->getId() ]
		));
	}catch(Error | Exception $e){}
}
$respond("002",$searchProductOnline($gtin));