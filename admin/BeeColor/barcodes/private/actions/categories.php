<?php
$categoryCollection = $_objectManager->get('\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory');
$categories = $categoryCollection->create();
$categories->addAttributeToSelect('*');

$res = [];
$childs = [];
foreach ($categories as $category) {
	try{
		if($category->getParentCategory()) $pid = $category->getParentCategory()->getId();
		else $pid = null;
	}catch(Error | Exception $e){
		$pid = null;
	}
	$res[$id = (string) $category->getId()] = [
		"name" => $name = $category->getName(),
		"parent" => $pid
	];
	if(!is_null($pid)){
		if(!is_array($childs[(string) $pid] ?? null)) $childs[(string) $pid] = [];
		$childs[(string) $pid][(string) $id] = ["name" => $name];
	}
}

$respond("001",$res);