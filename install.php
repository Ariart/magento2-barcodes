#!/usr/bin/php -q
<?php
/**
 * @return bool True if in CLI contect, false if in a webserver context
 */
function inCli():bool{
	if(defined('STDIN')) return true;
	if(php_sapi_name() === 'cli') return true;
	if(array_key_exists('SHELL', $_ENV)) return true;
	if(empty($_SERVER['REMOTE_ADDR']) && !isset($_SERVER['HTTP_USER_AGENT']) && count($_SERVER['argv']) > 0)
		return true;
	if(!array_key_exists('REQUEST_METHOD', $_SERVER) ) return true;
	return false;
}

/**
 * @param array $arr
 * @param int   $i
 * @return mixed
 * @throws Exception
 */
function throwIfOutOfBounds(array $arr, int $i){
	if(!isset($arr[$i])) throw new Exception("Undefined index $i !");
	return $arr[$i];
}

/**
 * @param string $cmd
 */
function execIO(string $cmd){
	$descriptorspec = array(
		0 => array("pipe", "r"),   // stdin is a pipe that the child will read from
		1 => array("pipe", "w"),   // stdout is a pipe that the child will write to
		2 => array("pipe", "w")    // stderr is a pipe that the child will write to
	);
	$process = proc_open(
		$cmd,
		$descriptorspec,
		$pipes, realpath('./'),
		array()
	);
	if (is_resource($process)) while($s = fgets($pipes[1])) print $s;
	proc_close($process);
}

$confs = require_once __DIR__."/admin/BeeColor/barcodes/private/config/conf.php";

$i = 0;
$args = array_slice($argv,1);
$opts = [
	"root" => "/var/www/magento",
	"user" => "www-data",
	"group" => "www-data",
	"chmod" => "0777",
	"data" => $confs["data"] ?? "/root/BeeColor/barcodes",
	"clean" => false,
	"logs" => [
		"file-logging" => false,
		"file" => "/var/log/magento-beecolor-barcodes.log",
		"WARN" => [ "color" => 33, "text" => "WARN" ],
		"LOG" => [ "color" => 0, "text" => "INFO" ],//0 : default
		"ERR" => [ "color" => 31, "text" => "ERR" ],//31 : rouge
		"FATAL" => [ "color" => 41, "text" => "FATAL ERROR" ],//41 : beackground rouge
		"OK" => ["color" => 32, "text" => "SUCESS"],//32 : vert
		"LOG_TEMPLATE" => "\e[@colorm[@date] [@type] @message\e[0m\n"
	]
];

$moduleName = "BeeColor_Barcodes";
$modulePath = "admin/BeeColor/barcodes";
$magentoCli = "php \"{$opts["root"]}/bin/magento\" " ;
$moduleBasePath = explode("/",$modulePath)[0];
$magentoModuleDir = "{$opts["root"]}/app/code/BeeColor/Barcodes";

$execMagentoCommands = function() use(&$magentoCli, &$opts):void{
	execIO("$magentoCli setup:upgrade");
	execIO("$magentoCli setup:di:compile");
	execIO("$magentoCli setup:static-content:deploy -f");
	execIO("$magentoCli setup:static-content:deploy fr_FR -f");
	execIO("chmod -R {$opts["chmod"]} {$opts["root"]}");
	execIO("chown -R {$opts["user"]}:{$opts["group"]} {$opts["root"]}");
};

//log constants
define("WARN",$opts["logs"]["WARN"]); //33 : orange
define("LOG", $opts["logs"]["LOG"]);//0 : default
define("ERR", $opts["logs"]["ERR"]);//31 : rouge
define("FATAL", $opts["logs"]["FATAL"]);//41 : beackground rouge
define("OK", $opts["logs"]["OK"]);//32 : vert
define("LOG_TEMPLATE", $opts["logs"]["LOG_TEMPLATE"]);

$logFile = null;
$quiet = false;

$log = function(string $message, array $params=LOG) use (&$logFile, $quiet){
	$handles = [];
	if(!$quiet) $handles[] = STDOUT;
	if(is_resource($logFile)) $handles[] = $logFile;
	foreach($handles as $h) fwrite($h,str_replace(
		["@color", "@date", "@type", "@message"],
		[
			$params["color"],
			\DateTime::createFromFormat('0.u00 U',microtime())
			         ->setTimezone(new \DateTimeZone(date_default_timezone_get()))
			         ->format("d-m-Y H:i:s.u"),
			$params["text"],
			$message
		],
		LOG_TEMPLATE
	));
};

//Change the default error handler to throw error on warnings too.
set_error_handler(function(string $errno, string $errstr, string $errfile, string $errline) use (&$log){
	$log(new ErrorException($errstr, 0, $errno, $errfile, $errline),FATAL);
	exit(1);
},E_ALL);

if(!inCli()) throw new Error("This script must run in CLI mode !");

try{
	$noPrompt = false;
	$uninstall = false;
	$autoMagentoCli = true;
	$magentoKey = $confs["admin_key"] ?? '';

	if(empty($magentoKey) && file_exists($opts["root"]."/app/etc/env.php")){
		$tmpConf = require $opts["root"]."/app/etc/env.php";
		$magentoKey = explode("_",$tmpConf["backend"]["frontName"])[1] ?? '';
		if(!empty($magentoKey)) $log("Backend url key retrieved : $magentoKey",OK);
	}

	while($i < count($args)){
		try{
			switch($args[$i]){
				case "-r" : case "-root" :
					$i++; $opts["root"] = throwIfOutOfBounds($args,$i);
					break;
				case "-u": case "-user":
					$i++; $opts["user"] = throwIfOutOfBounds($args,$i);
					break;
				case "-g": case "-group":
					$i++; $opts["group"] = throwIfOutOfBounds($args,$i);
					break;
				case "-c": case "-chmod":
					$i++; $opts["chmod"] = throwIfOutOfBounds($args,$i);
					break;
				case "-d": case "-data":
					$i++; $opts["data"] = throwIfOutOfBounds($args,$i);
					break;
				case "-clean":
					$opts["clean"] = true;
					break;
				case "-q" : case "-quiet" :
					$quiet = true;
					break;
				case "-n" : case "-no-prompt" :
					$noPrompt = true;
					break;
				case "-f" : case "-file-logs" :
					$opts["logs"]["file-logging"] = true;
					break;
				case "-l" : case "-logs-file" :
					$opts["logs"]["file-logging"] = true;
					$i++; $opts["logs"]["file"] = throwIfOutOfBounds($args,$i);
					break;
				case "-remove" :
					$uninstall = true;
					break;
				case "-m" : case "-disable-magento-cli" :
					$autoMagentoCli = false;
					break;
				case "-k" :case "-key" :
					$i++; $magentoKey = throwIfOutOfBounds($args,$i);
					break;
				default : throw new InvalidArgumentException("Unknown argument ".$args[$i]);
			}
		}catch(InvalidArgumentException $e){
			throw new Error($e);
		}catch (Exception $e){
			throw new Error($args[$i-1]." : a value must be specified.");
		}
		$i++;
	}

	if(!empty($magentoKey)){
		$nPath = str_replace("admin/","admin_$magentoKey/", $modulePath);
		$log("Magento backend url key set. $modulePath becomes $nPath.");
		$modulePath = $nPath;
	}

	/**
	 * @param string      $ask
	 * @param null|string $aborted
	 * @param bool        $exitIfAbort
	 * @return bool|null
	 */
	$prompt = function(string $ask, ?string $aborted=null, bool $exitIfAbort = true) use(&$log):?bool{
		fwrite(STDOUT,$ask);
		if(!filter_var(preg_replace(["/^y$/","/^n$/"],["yes","no"],fgets(STDIN)), FILTER_VALIDATE_BOOLEAN)){
			$log($aborted ?? "Aborted",ERR);
			if($exitIfAbort) exit(0);
			else return false;
		} else return true;
	};

	if($opts["logs"]["file-logging"]){
		if(!is_dir(dirname($opts["logs"]["file"])))
			mkdir(dirname($opts["logs"]["file"]), $opts["chmod"], true);
		fopen($opts["logs"]["file"],"w");
	}

	$update = is_dir($opts["root"]."/$modulePath");
	if($uninstall){
		if(!$update){
			$log("The barcodes module is not installed !",FATAL);
			exit(0);
		}
		if(!$noPrompt) $prompt("Do you really want to uninstall the barcodes module ? (all data will be removed !) (y/n) : ");
		$log("The barcodes module will be uninstalled...");
		execIO("$magentoCli modules:disable $moduleName");
		execIO("rm -rf \"{$opts["root"]}/$modulePath\"");
		$log("{$opts["root"]}/$modulePath removed.",OK);
		execIO("rm -rf \"{$opts["data"]}\"");
		$log("{$opts["data"]} removed.",OK);
		if($autoMagentoCli) $execMagentoCommands();
		else $log("Auto magento commands have been disabled.",WARN);
		$log("Barcodes module uninstalled.",OK);
		exit(0);
	}
	$log(($update?"Updating":"Installing")." module barcodes...");

	if($update && !$noPrompt){
		$prompt("Do you really want to update {$opts["root"]}/$modulePath ? (y/n) : ");
		$log($opts["root"]."/$modulePath will be updated.", OK);
	}
	if($update && $opts["clean"]){
		if(is_dir($opts["data"])){
			if(!$noPrompt) $prompt("Do you really want to clean {$opts["data"]} ? (y/n) : ");
			execIO("rm -rf \"{$opts["data"]}\"");
			$log($opts["data"]." removed.",OK);
		}
	}
	if($update && $opts["clean"]) {
		execIO("rm -rf \"{$opts["root"]}/$modulePath\"");
		$log($opts["root"]."/$modulePath removed.",OK);
	}

	if(!is_dir($opts["root"])){
		mkdir($opts["root"],$opts["chmod"],true);
		$log("{$opts["root"]} created",OK);
	}

	$importAdminDest = $opts["root"]."/admin".(!empty($magentoKey)?"_$magentoKey":"");
	if(!is_dir($importAdminDest)) mkdir($importAdminDest);
	execIO("cp -r ".__DIR__."/$moduleBasePath/* \"$importAdminDest\"");
	$log(__DIR__."/$moduleBasePath/* imported into $importAdminDest",OK);

	if(is_dir($magentoModuleDir) && $opts["clean"]){
		execIO("rm -rf $magentoModuleDir");
		$log("$magentoModuleDir cleaned up.",OK);
	}
	execIO("cp -R \"".__DIR__."/app\" \"{$opts["root"]}\"");
	$log(__DIR__."/app imported into {$opts["root"]}",OK);
	if(!empty($magentoKey)){
		$jsPath = $opts["root"]."/app/code/BeeColor/Barcodes/view/adminhtml/web/js/loader.js";
		$content = file_get_contents($jsPath);
		file_put_contents($jsPath,preg_replace("#(/\*@\*/.*)(/admin/)(.*/\*@\*/)#","$1/admin_$magentoKey/$3",$content));
		$log("$jsPath modified to take magento admin key into consideration.",OK);
	}

	if(!is_dir($opts["data"])){
		mkdir($opts["data"],$opts["chmod"],true);
		$log("{$opts["data"]} created",OK);
	}

	execIO("chmod -R {$opts["chmod"]} \"{$opts["root"]}/$modulePath\"");
	execIO("chmod -R {$opts["chmod"]} \"{$opts["data"]}\"");
	execIO("chown -R {$opts["user"]}:{$opts["group"]} \"{$opts["root"]}/$modulePath\"");
	execIO("chown -R {$opts["user"]}:{$opts["group"]} \"{$opts["data"]}\"");
	$log("Permissions {$opts["chmod"]} {$opts["user"]}:{$opts["group"]} set recursively to {$opts["data"]}, {$opts["root"]}/$modulePath",OK);

	if($autoMagentoCli) $execMagentoCommands();
	else $log("Auto magento commands have been disabled.",WARN);

	$log(($update?"Update":"Installation")." done.",OK);
}catch(Exception | Error $e){
	$log($e,FATAL);
	exit(1);
}